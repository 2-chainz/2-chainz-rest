# 2 Chainz Rest

API for 2 Chainz quotes an aliases built using Python and Azure Functions. More project info at [2.chainz.rest](https://2chainz.ansonbiggs.com).

## Endpoints

### Quote

Returns a random 2 Chainz Quote in `json` format like the following example:

send a `get` request to `https://chainz-rest.azurewebsites.net/quote`

```json
{
  "quote": "I got a pocket full of money, it got me walking all slew-foot"
}
```

#### Parameters

This endpoint also supports an optional `batch` parameter to get more than one quote per request. Maximum quotes that the endpoint will return is the amount of quotes in [quotes.py](quote/quotes.py) and is subject to change. An example return from [https://chainz-rest.azurewebsites.net/quote?batch=2](https://chainz-rest.azurewebsites.net/quote?batch=2):

```json
{
  "quotes": [
    "I'm in the kitchen. Yams errrrrwhere.",
    "Started from the trap, now I rap"
  ]
}
```

[Source Code](quote/__init__.py)

### Alias

Returns a random 2 Chainz alias in `json` format. The return values are weighted and a full list can be seen in [aliases.py](alias/aliases.py).

send a `get` request to `https://chainz-rest.azurewebsites.net/alias`

```json
{
  "alias": "Dos Cadenas"
}
```

[Source Code](alias/__init__.py)
