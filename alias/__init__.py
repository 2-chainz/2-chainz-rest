import logging
import json
import azure.functions as func
import random
from . import aliases


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info("Python HTTP trigger function processed a request.")
    return func.HttpResponse(json.dumps({"alias": random.choice(aliases.aliases)}))
