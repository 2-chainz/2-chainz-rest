# Name and frequency it will be used
aliases_list = [
    ("2 Chainz", 50),
    ("Tity Boi", 10),
    ("Daniel Son; Necklace Don", 10),
    ("Drenchgod", 5),
    ("Tauheed Epps", 1),
    ("Deuce", 20),
    ("Dos Cadenas", 20),
    ("Hair Weave Killer", 20),
    ("Hair Weave Killa", 10),
    ("JEFE TONI", 10),
    ("JEFE", 5),
    ("TONI", 25),
    ("BIG TONI", 10),
    ("PRETTI TONI", 1),
    ("TONÉ", 1),
    ("2 TONE TONI", 5),
    ("TONI 2-STIX", 1),
    ("TONI KEYS", 1),
    ("FAT TONI", 10),
]

aliases = []
for a in aliases_list:
    for i in range(a[1]):
        aliases.append(a[0])
