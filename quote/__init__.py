import logging
import json
import azure.functions as func
import random
from . import quotes


def main(req: func.HttpRequest) -> func.HttpResponse:
    batch = req.params.get("batch")
    random.shuffle(quotes.quotes)
    if batch is not None:
        try:
            batch = int(batch)
        except ValueError:
            return func.HttpResponse(
                "'batch' param must be an integer.", status_code=400
            )
        return func.HttpResponse(json.dumps({"quotes": quotes.quotes[0:batch]}))

    logging.info("Python HTTP trigger function processed a request.")
    return func.HttpResponse(json.dumps({"quote": quotes.quotes[0]}))
